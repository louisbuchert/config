Requirements:
- delta
- clang
- ripgrep
- lazygit
- nvim > v0.9.0
- packer.nvim
- fzf
- oh-my-zsh
- zsh-autosuggestions

