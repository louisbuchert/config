export PATH="$HOME/.local/bin:$PATH"

alias n="nvim.appimage"
alias gs="git status"
alias gl="git log"

export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="agnoster"

plugins=(
	git
	z 
	zsh-autosuggestions 
	zsh-interactive-cd
)

source $ZSH/oh-my-zsh.sh

