
require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'
	use 'ellisonleao/gruvbox.nvim'
	use 'lewis6991/gitsigns.nvim'
	use 'kdheepak/lazygit.nvim'
	use 'junegunn/vim-peekaboo'
	use 'ethanholz/nvim-lastplace'
	use 'vivien/vim-linux-coding-style'
	use {
  		'nvim-telescope/telescope.nvim',
  		requires = { 
			'nvim-lua/plenary.nvim',
			'BurntSushi/ripgrep',
			'sharkdp/fd',
			'nvim-treesitter/nvim-treesitter'
		}
	}
	use {
		'nvim-telescope/telescope-file-browser.nvim',
		'nvim-telescope/telescope-live-grep-args.nvim',
		'nvim-telescope/telescope-fzf-native.nvim',
		requires = {
			'nvim-telescope/telescope.nvim',
			'nvim-lua/plenary.nvim'
		}
	}
	use {
		'nvim-tree/nvim-tree.lua',
		requires = { 'nvim-tree/nvim-web-devicons' },
	}
	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'kyazdani42/nvim-web-devicons', opt = true }
	}
	use {
		'VonHeikemen/lsp-zero.nvim',
		requires = {
			-- LSP Support
			'neovim/nvim-lspconfig',             -- Required
			'williamboman/mason.nvim',           -- Optional
			'williamboman/mason-lspconfig.nvim', -- Optional
			-- Autocompletion
			'hrsh7th/nvim-cmp',         -- Required
			'hrsh7th/cmp-nvim-lsp',     -- Required
			'hrsh7th/cmp-buffer',       -- Optional
			'hrsh7th/cmp-path',         -- Optional
			'saadparwaiz1/cmp_luasnip', -- Optional
			'hrsh7th/cmp-nvim-lua',     -- Optional
			-- Snippets
			'L3MON4D3/LuaSnip',             -- Required
			'rafamadriz/friendly-snippets', -- Optional
		}
	}
end)

-- LSP-zero
local lsp = require('lsp-zero')
lsp.preset('recommended')
lsp.setup_servers({'clangd'})
lsp.setup()

-- Telescope
local telescope = require('telescope')
telescope.setup {
--	defaults = {
--		layout_stategy = "vertical"
--	},
	pickers = {
		buffers = {mappings = {
			i = {['<M-q>'] = 'delete_buffer'},
			n = {['<M-q>'] = 'delete_buffer'}
		}}
	}
}
--telescope.load_extension 'file_browser'
--telescope.load_extension 'live_grep_args'
--telescope.load_extension 'fzf'
--vim.keymap.set('n', 'ff', '<cmd>Telescope file_browser hidden=true<cr>')
vim.keymap.set('n', 'ff', '<cmd>Telescope find_files<cr>')
--vim.keymap.set('n', 'fg', '<cmd>Telescope live_grep_args search_dirs={"%:p"}<cr>')
vim.keymap.set('n', 'fg', '<cmd>Telescope live_grep<cr>')
vim.keymap.set('n', 'fs', '<cmd>Telescope grep_string<cr>')
vim.keymap.set('n', 'fb', '<cmd>Telescope buffers<cr>')
vim.keymap.set('n', 'fm', '<cmd>Telescope git_status<cr>')
vim.keymap.set('n', 'fh', '<cmd>Telescope help_tags<cr>')

-- Gitsigns
require('gitsigns').setup()
vim.keymap.set('n', 'hp', '<cmd>Gitsigns preview_hunk<cr>')
vim.keymap.set('n', 'hs', '<cmd>Gitsigns stage_hunk<cr>')
vim.keymap.set('n', 'hu', '<cmd>Gitsigns reset_hunk<cr>')
vim.keymap.set('n', ']h', '<cmd>Gitsigns next_hunk<cr>')
vim.keymap.set('n', '[h', '<cmd>Gitsigns prev_hunk<cr>')

-- NvimTree
require('nvim-tree').setup()
vim.keymap.set('n', '<C-t>', '<cmd>NvimTreeToggle<cr>')

-- Other
require('lualine').setup {options = {theme = 'gruvbox'}}
vim.cmd('colorscheme gruvbox')
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.number = true
vim.opt.cursorline = true
vim.opt.clipboard = 'unnamedplus'
vim.opt.background = 'dark'
vim.opt.foldmethod = 'syntax'
vim.opt.foldlevelstart = 99
vim.keymap.set('n', '<C-c>', '<cmd>nohls<cr>')
vim.keymap.set({'n', 'i'}, '<C-q>', '<cmd>q<cr>')
vim.keymap.set({'n', 'i'}, '<C-x>', '<cmd>x<cr>')
vim.keymap.set('n', '<C-s>', '<cmd>w<cr>')
vim.keymap.set('i', '<C-s>', '<esc><cmd>w<cr>')
vim.keymap.set('i', '<C-w>', '<esc><C-w>')

